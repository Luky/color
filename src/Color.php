<?php declare(strict_types = 1);

namespace Luky\Color;

use Nette\Utils\Strings;

class Color
{
	protected int $red;
	protected int $green;
	protected int $blue;
	protected int $alpha;


	public function __construct(int $red, int $green, int $blue, int $alpha = 255)
	{
		$this->red = $red;
		$this->green = $green;
		$this->blue = $blue;
		$this->alpha = $alpha;
	}


	public static function createFromHex(string $hex): self
	{
		if(Strings::startsWith($hex, '#')) {
			$hex = Strings::after($hex, '#');
		}

		$red = null;
		$green = null;
		$blue = null;
		$alpha = 'FF';

		if (\strlen($hex) === 3) {
			$red = Strings::substring($hex, 0, 1);
			$green = Strings::substring($hex, 1, 1);
			$blue = Strings::substring($hex, 2, 1);
		} elseif (\strlen($hex) === 6) {
			$red = Strings::substring($hex, 0, 2);
			$green = Strings::substring($hex, 2, 2);
			$blue = Strings::substring($hex, 4, 2);
		} elseif (\strlen($hex) === 8) {
			$red = Strings::substring($hex, 0, 2);
			$green = Strings::substring($hex, 2, 2);
			$blue = Strings::substring($hex, 4, 2);
			$alpha = Strings::substring($hex, 6, 2);
		} else {
			throw new \LogicException();
		}

		return new self(
			\hexdec($red),
			\hexdec($green),
			\hexdec($blue),
			\hexdec($alpha)
		);
	}

	public function getRed(): int
	{
		return $this->red;
	}


	public function getGreen(): int
	{
		return $this->green;
	}


	public function getBlue(): int
	{
		return $this->blue;
	}


	public function getAlpha(): int
	{
		return $this->alpha;
	}


	public function getRGB(): array
	{
		return [
			'r' => $this->red,
			'g' => $this->green,
			'b' => $this->blue,
		];
	}


	public function getRGBA(): array
	{
		return [
			'r' => $this->red,
			'g' => $this->green,
			'b' => $this->blue,
			'a' => $this->alpha,
		];
	}


	public function getHex(bool $withAlpha = false): string
	{
		return $this->createHex(
			$this->red,
			$this->green,
			$this->blue,
			$this->alpha,
			$withAlpha
		);
	}


	private function createHex(
		int $red,
		int $green,
		int $blue,
		int $alpha,
		bool $withAlpha
	): string
	{
	}
}
